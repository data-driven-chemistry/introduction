# Data Driven Chemistry Introduction


### What you will learn

* Interact with a Jupyter notebook
* Declare variables
* Print Variables
* Learn how to look at documentation
* Collections
* Read data from a file
* Plotting with Pandas
* Loops, Conditionals,
* Functions
* Learn about some best practices when programming

### Material
[Lecture_00](Link to lecture) -- An introductory lecture on computing in the sciences. Please watch it in your own time.   
[Session_01](session1/Session_1.0.ipynb) -- Introduction to Juypter notebooks and First Steps in Python     
[Session_02](session2/Session_2.0.ipynb) -- Loops, Conditionals and plotting    
[Session_03](session3/Session_3.0.ipynb) -- Writing functions     

### Further reading

Please check back shortly for more reading material. Or just use your friend [Google](google.com).

### Contact:

Dr Antonia Mey -- antonia.mey@ed.ac.uk    
Dr Claire Hobday -- Claire.Hobday@ed.ac.uk    
If you have any issues or concerns about this part of the course, please get in touch with either Dr Mey or Dr Hobday. 


### Manual Installation:
If you wish to install the workshop material manually, you will need the following packages (including all their dependencies):

- `jupyter`
- `pandas`
- `nglview`
- `nbexamples`
- `jupyter_contrib_nbextensions`

This can be done, for example, with conda:

1. Create a new conda environment:

```bash
conda create -n dd_chem_intro
conda activate dd_chem_intro
```

2. In the newly activated environment you can now install all the requirements:

```bash
conda install -c conda-forge jupyter nglview nbexamples jupyter_contrib_nbextensions pandas
```

After installing `jupyter_contrib_nbextensions`, you need to activate the `toc2` and `exercise2` extensions:

```bash
jupyter nbextension enable toc2/main
jupyter nbextension enable exercise2/main
```

Now all remains is to clone the repository to get the tutorial notebooks:

```bash
git clone git@git.ecdf.ed.ac.uk:data-driven-chemistry/introduction.git
```


### Licence:
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" title='This work is licensed under a Creative Commons Attribution 4.0 International License.' align="left"/></a>
