{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Populating the interactive namespace from numpy and matplotlib\n"
     ]
    }
   ],
   "source": [
    "import numpy as np\n",
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Session 3B - Functions\n",
    "---"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Jupyter cheat sheet**:\n",
    "- to run the currently highlighted cell, hold <kbd>&#x21E7; Shift</kbd> and press <kbd>&#x23ce; Enter</kbd>;\n",
    "- to get help for a specific function, place the cursor within the function's brackets, hold <kbd>&#x21E7; Shift</kbd>, and press <kbd>&#x21E5; Tab</kbd>;"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "[1. Returning Values](#returns)   \n",
    "[2. Functions Calling Functions](#function#function)   \n",
    "[3. What is a good programming style?](#style)   "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "button": false,
    "new_sheet": false,
    "run_control": {
     "read_only": false
    }
   },
   "source": [
    "## Returning values from a function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In all the examples previously, we have been using the `print()` to output our information from the function. Programmers rarely use functions to print to the screen as the `print()` function already does that for us. Normally functions are used to do something to the argument that is passed to it then return the result of that something.\n",
    "\n",
    "Consider the `len()` function:\n",
    "```python\n",
    "seq_len = len( 'ACGTGGGTA' )\n",
    "```\n",
    "In this code we pass a DNA sequence as a string to `len()` which  **returns** its length which we can assign to a variable, in this case called `seq_len`. \n",
    "\n",
    "This is a more versatile use of functions than just printing because we can now use the value of `seq_len` for many other purposes.\n",
    "\n",
    "In the code below is a function called `square_it()` that takes a number, squares it and **returns** the square. \n",
    "\n",
    "<div class=\"alert alert-danger\">\n",
    "Run it to see what happens.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "81.0\n"
     ]
    }
   ],
   "source": [
    "def square_it(x):\n",
    "    \"\"\"Return the square of the number x\"\"\"\n",
    "\n",
    "    square = x**2\n",
    "    return square\n",
    "\n",
    "\n",
    "# Call \"square_it()\" with the argument 9.\n",
    "# Assign the returned value (which is 81) to the variable \"y\".\n",
    "y = square_it(9.)\n",
    "print(y)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function `square_it()` takes a number and stores it in the parameter `x`. `x` is squared (remember `**` is the power operator, i.e., `x` to the power 2 is x squared.). The square of `x` is assigned to the variable `square`. Finally, the function returns the value in `square` back to the main program where it is assigned to the variable `y`, and the printed.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this next example, we input 3 variables, `deltaG`, `R` and `T` and return one value `K`, the equilibrium constant.\n",
    "These are related through the equation:\n",
    "\n",
    "\\begin{equation}\n",
    "K = \\exp (\\frac{-\\Delta G}{RT}) \n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The equilibrium constant is 3711.043\n"
     ]
    }
   ],
   "source": [
    "#we use numpy as np and use np.exp \n",
    "#This gives us access to the exponential function within numpy.\n",
    "def equilibrium_constant(deltaG, R, T):\n",
    "    K = np.exp(-deltaG / (R * T))\n",
    "    return K\n",
    "\n",
    "deltaG = -20.5 #kJ/mol\n",
    "R = 8.314 #J/Kmol\n",
    "R = 8.314 * 0.001 #kJ/Kmol\n",
    "T = 300 #K\n",
    "\n",
    "K = equilibrium_constant(deltaG, R, T)\n",
    "print('The equilibrium constant is {:.3f}'.format(K))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions can return more than one value"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The above function returned just a single value, but functions can return as many values as we like.\n",
    "\n",
    "See the example below for finding the roots of a quadratic equation. \n",
    "\n",
    "$$ y = ax^2 + bx + c $$\n",
    "\n",
    "where, $a = 1$, $b = 4$, and $c = 2$. \n",
    "\n",
    "And of course the postive and negative roots are:\n",
    "$$ x+ = \\frac{-b + \\sqrt{b^2 - 4ac}}{2a} $$\n",
    "\n",
    "$$ x- = \\frac{-b - \\sqrt{b^2 - 4ac}}{2a} $$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "The positive root is -0.5857864376269049\n",
      "The negative root is -3.414213562373095\n"
     ]
    }
   ],
   "source": [
    "#Here we are using numpy's squareroot function np.sqrt\n",
    "def getRoots(a, b, c):\n",
    "    x_plus = (-b + np.sqrt(b ** 2 - 4 * a * c)) / (2 * a)\n",
    "    x_minus = (-b - np.sqrt(b ** 2 - 4 * a * c)) / (2 * a)\n",
    "    return x_plus, x_minus\n",
    "\n",
    "x_plus, x_minus = getRoots(1, 4, 2)\n",
    "print(f'The positive root is {x_plus}')\n",
    "print(f'The negative root is {x_minus}')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The function returns both values stored in the variables `x_plus` and `x_minus` simultaneously back to the main program."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Functions can call functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we've seen a function can be called form inside another function. The code below shows an example we've already seen."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello Bob\n"
     ]
    }
   ],
   "source": [
    "def say_hello(name):\n",
    "    \"\"\"Print Hello name\"\"\"\n",
    "    \n",
    "    print( f'Hello {name}')\n",
    "    \n",
    "say_hello('Bob')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this example we call the function `say_hello()` which we have written. Our function calls the inbuilt function `print()`.\n",
    "\n",
    "But our function can also call another function that **we** have written.\n",
    "\n",
    "Remember the function which converts fahrenheit to celsius?\n",
    "```python\n",
    "def fahr_to_celsius(temp):\n",
    "    return ((temp - 32) * (5/9))\n",
    "```\n",
    "Let's write another one which will convert celsius to kelvin:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fahr_to_celsius(temp):\n",
    "    return ((temp - 32) * (5/9))\n",
    "def celsius_to_kelvin(temp_c):\n",
    "    return temp_c + 273.15"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, let's convert Farhenheit to Kelvin. \n",
    "\n",
    "We could write out the formula, but we don’t need to. Instead, we can compose the two functions we have already created:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "def fahr_to_kelvin(temp_f):\n",
    "    temp_c = fahr_to_celsius(temp_f)\n",
    "    temp_k = celsius_to_kelvin(temp_c)\n",
    "    return temp_k"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "boiling point of water in Kelvin: 373.15\n"
     ]
    }
   ],
   "source": [
    "print('boiling point of water in Kelvin:', fahr_to_kelvin(212.0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Exercises"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3.7\n",
    "\n",
    "Modify your code from Exercise 3.5 so that the function `average()` returns the average of a list of numbers rather than printing it."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [],
   "source": [
    "Titre_values = [22.2, 23.3, 22.3, 22.4, 22.4, 23.0, 22.0, 22.1, 21.9]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "ave = average( Titre_values )\n",
    "print(ave)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3.8\n",
    "Now write a program that will not only calculate the average, but also the standard deviation of these values and return both values. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "\n",
    "ave , std_dev = ave_std_dev (Titre_values)\n",
    "print(ave)\n",
    "print(std_dev)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Exercise 3.9\n",
    "\n",
    "Write a function ( `split_name`)that splits someone's full name into a forename and surname and returns both of these separately. Test it out of the list `full_names`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "full_names = ['Harry Potter', 'Hermione Granger', 'Ronald Weasley']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "\n",
    "for name in full_names:\n",
    "    forename, surname = split_name(name)\n",
    "    print(f'{surname.upper()} {forename}')"
   ]
  }
 ],
 "metadata": {
  "anaconda-cloud": {},
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  },
  "toc": {
   "base_numbering": 1,
   "nav_menu": {},
   "number_sections": false,
   "sideBar": true,
   "skip_h1_title": false,
   "title_cell": "Table of Contents",
   "title_sidebar": "Contents",
   "toc_cell": false,
   "toc_position": {},
   "toc_section_display": true,
   "toc_window_display": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
